import React, { Component } from "react";

class Greet extends Component {
    render(){
        const {name, secondName} = this.props
        //const {state1, state2} = this.state
        return <h1>Welcome {name} {secondName}</h1>
    }
}

export default Greet