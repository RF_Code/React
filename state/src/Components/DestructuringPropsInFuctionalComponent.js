import React from 'react'

const Welcome = ({name, secondName}) => //props
{
    ///const {name, secondName} = props -another way
    return (
        <div>
            <h1>Welcome {name} {secondName}</h1>
        </div>
    ) 
}

export default Welcome 
