import React, { Component } from "react";

class Information extends Component {

    constructor() {
        super()
        this.state = {
            information : 'Hallo'
        }
    }

    changeInformation(){
        this.setState({
            information: 'Hallo there'
        })
    }

    render(){
        return(
            <div>
                <h1>{this.state.information}</h1>
                <button onClick={() => this.changeInformation()}>Change message</button>
            </div>
        ) 
    }
}

export default Information