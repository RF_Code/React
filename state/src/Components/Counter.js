import React, { Component } from "react";

class Counter extends Component {

    constructor(props){
        super(props)

        this.state = {
            count: 0
        }
    }


    onIncrease(){
        this.setState({
            count: this.state.count + 1
        },
        () => {console.log('Callback value', this.state.count)})
        console.log(this.state.count)
    }

    onDecrease(){
        this.setState({
            count: this.state.count - 1
        },
        () => {console.log('Callback value', this.state.count)})
        console.log(this.state.count)
    }

    increase(){
        this.setState(prevState => ({
            count: prevState.count + 1
        }))
        console.log(this.state.count)
    }

    onIncreaseByFive(){
        this.increase()
        this.increase()
        this.increase()
        this.increase()
        this.increase()

        // jakby wywoływało sie metode onIncrease zwiekszyłby sie licznik tylko o jeden poniewaz Reakt uznałby to za jeden blok.
        // Kiedy jest użyty prevSate React widzi to jako pojedyńcze metody
    }

    render(){
        return(
            <div>
                Count is {this.state.count}
                <button onClick={() => this.onIncrease()}>Increase</button>
                <button onClick={() => this.onDecrease()}>Decrease</button>

                <button onClick={() => this.onIncreaseByFive()}>IncreaseByFive</button>
            </div>
            
        ) 
    }
}

export default Counter