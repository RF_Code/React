import Information from './Components/Information';
import './App.css';
import Counter from './Components/Counter';
import Welcome from './Components/DestructuringPropsInFuctionalComponent';
import Greet from './Components/DestructuringStateInClassComponent';

function App() {
  return (
    <div className="App">
      <Information/>
      <Counter/>
      <Welcome name="Jan" secondName="Kowalski"></Welcome>
      <Greet name="Jan" secondName="Nowak"></Greet>
    </div>
  );
}

export default App;
