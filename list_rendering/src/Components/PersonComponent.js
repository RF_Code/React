import React from 'react'

function PersonComponent({person}) {
  return (
    <div>
      <h2>I am {person.name}. I have {person.age} years old. I know {person.skill}</h2>
    </div>
  )
}

export default PersonComponent
