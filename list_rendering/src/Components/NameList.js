import React from 'react'
import PersonComponent from './PersonComponent'

//An index as a key in lists is an anti-pattern because when adding, deleting, filtering, sorting, it loses the index positions or values ​​​​under this index

function NameList() {
    const names = ['Rafal', 'Alicja', 'Bartek']
    const namesList = names.map((name, index) => <h2 key={index}>{name + ' second way'}</h2>)
    const persons = [
        {
            id: 1,
            name: 'Rafal',
            age: 31,
            skill: 'React'
        },
        {
            id: 2,
            name: 'Alicja',
            age: 26,
            skill: 'C#'
        },
        {
            id: 3,
            name: 'Bartek',
            age: 45,
            skill: 'Python'
        }
    ]
    const personList = persons.map((person, index) => <h2 key={index}>I am {person.name}. I have {person.age} years old. I know {person.skill}</h2>)
    const personComponentList = persons.map(person => (
        <PersonComponent key={person.name} person={person}/>
    ))

  return (
    <div>
      <div>
        {
            names.map((name, index) => <h2 key={index}> {index} {name}</h2>)
        }
      </div>
      ________________________________________
      <div>
        {
            namesList
        }
      </div>
      ________________________________________
      <div>
        {
            personList
        }
      </div>
      ________________________________________
      <div>
        {
            personComponentList
        }
      </div>
    </div>
    
  )
}

export default NameList

