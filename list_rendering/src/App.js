import logo from './logo.svg';
import './App.css';
import NameList from './Components/NameList';

function App() {
  return (
    <div className="App">
      <NameList/>
    </div>
  );
}

export default App;
