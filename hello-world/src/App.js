import './App.css';
import GreatComponent from './Components/Greet';
import { GreatConst } from './Components/GreetConst';
import Welcome from './ClassComponents/Welcome';

function App() {
  return (
    <div className="App">
      <GreatComponent/>
      <GreatConst/>
      <Welcome/>
    </div>
  );
}

export default App;