import React from 'react'

const Welcome = () => 
{
    return React.createElement(
        'div', 
        {Id: 'Title', ClassName: 'dummy'}, //Add to div ID, className. Important -> CamelCase
        React.createElement('h1', null, 'Hello JSX'))
}

export default Welcome 