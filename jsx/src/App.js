import WelcomeJSX from './Components/WelcomeJSX';
import './App.css';

function App() {
  return (
    <div className="App">
      <WelcomeJSX/>
    </div>
  );
}

export default App;
