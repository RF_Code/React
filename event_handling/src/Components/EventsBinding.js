import React, { Component } from 'react'

class EventsBinding extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         message: 'Hallo'
      }

      this.clickHandler = this.clickHandler.bind(this)
    }

    clickHandler(){

        // throws an error because it requires binding line 26. 
        //There must be .bind(this). Another way is set in contructor this.clickHandler = this.clickHandler.bind(this) and at button set only this.clickHandler
        this.setState({
            message: 'Bye'
        })
        console.log(this)
    }

    eventHandler = () => {
        this.setState({
            message: 'Goodbye'
        })
    }
    
  render() {
    return (
      <div>
        <div>{this.state.message}</div>
        <button onClick={this.clickHandler.bind(this)}>Click Binding first aprouch</button>
        <button onClick={this.clickHandler}>Click Binding way two</button>
        <button onClick={this.eventHandler}>Click Binding way three</button>
        <button onClick={() => this.clickHandler()}>Click Binding by arrow function</button>
      </div>
    )
  }
}

export default EventsBinding
