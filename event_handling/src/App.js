import './App.css';
import Events from './Components/Events';
import EventsBinding from './Components/EventsBinding';
import EventsHandlerInClass from './Components/EventsHandlerInClass';

function App() {
  return (
    <div className="App">
      <Events/>
      <EventsHandlerInClass/>
      <EventsBinding/>
    </div>
  );
}

export default App;
