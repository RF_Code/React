import logo from './logo.svg';
import './App.css';
import UserGreeting from './Components/UserGreeting';
import MessageInformation from './Components/MessageInformation';
import Welcome from './Components/Welcome';
import ShowInformation from './Components/ShowInformation';

function App() {
  return (
    <div className="App">
      <UserGreeting/>
      <MessageInformation/>
      <Welcome/>
      <ShowInformation/>
    </div>
  );
}

export default App;
