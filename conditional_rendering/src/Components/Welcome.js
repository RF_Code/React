import React, { Component } from 'react'

export class Welcome extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         isLoggedIn: true
      }
    }
    

  render() {
    return (
        this.state.isLoggedIn ? 
        <div>Welcome</div> :
        <div>Welcome XYZ</div>
    )
  }
}

export default Welcome
