import React, { Component } from 'react'

class MessageInformation extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         isLoggedIn: true
      }
    }
    

  render() {

    let message
    if (this.state.isLoggedIn)
    {
        message = <div>Welcome Rafał</div>
    }
    else
    {
        message = <div>I don't know you</div>
    }

    return (
      <div>
        {message}
      </div>
    )
  }
}

export default MessageInformation
