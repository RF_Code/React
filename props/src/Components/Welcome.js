import React from 'react'

const Welcome = (props) => 
{
    return (
        <div>
            <h1>Welcome {props.name} {props.secondName}</h1>
            {props.children}
        </div>
    ) 
}

export default Welcome 