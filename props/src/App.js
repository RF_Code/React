import Welcome from './Components/Welcome';
import './App.css';
import Greet from './Components/Greet';

function App() {
  return (
    <div className="App">
      <Welcome name="X" secondName="Z">
        <p>This is children</p>
      </Welcome>
      <Welcome name="Jan" secondName="Kowalski">
        <button>Action</button>
      </Welcome>
      <Greet name="Jan" secondName="Nowak"></Greet>
    </div>
  );
}

export default App;
