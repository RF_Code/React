import React, { Component } from 'react'
import ChildComponet from './ChildComponet'

class ParentComponent extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         parentName: 'Parent'
      }

      this.greetParent = this.greetParent.bind(this)
      this.greetParentFromChild = this.greetParentFromChild.bind(this)
    }
    
    greetParent() {
        //alert('Hello' + this.state.parentName)
        alert(`Hello ${this.state.parentName}`)
    }

    greetParentFromChild(childName) {
        //alert('Hello' + this.state.parentName)
        alert(`Hello ${this.state.parentName} from ${childName}`)
    }

  render() {
    return (
      <div>
       <ChildComponet greetHandler={this.greetParent} greetFromChildHandler = {this.greetParentFromChild}/>
      </div>
    )
  }
}

export default ParentComponent
