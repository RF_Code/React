import React from 'react'

function ChildComponet(props) {
  return (
    <div>
      <button onClick={props.greetHandler}>Greet Parent without parameters</button>
      <button onClick={() => props.greetFromChildHandler('Child')}>Greet Parent with parameters</button>
    </div>
  )
}

export default ChildComponet
