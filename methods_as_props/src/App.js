import './App.css';
import ParentComponent from './Componets/ParentComponent';

function App() {
  return (
    <div className="App">
      <ParentComponent/>
    </div>
  );
}

export default App;
